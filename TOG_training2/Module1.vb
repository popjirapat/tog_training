﻿Imports System.Data.SqlClient
Imports DevDee.ApiHelper
Imports Newtonsoft.Json

Module Module1

    Sub Main()
        'กำหนดตัวแปร ที่ Api ต้องการ
        Dim PathUrl As String = "http://nttrkspp.novacel-optical.com/WebService/SRV_trackingSupply.php"
        Dim DataText As String = ""
        Dim UserName As String = "wsTOG"
        Dim PassWord As String = "C01dq1o-3zjf"
        Dim BAsicAuth As Boolean = True

        'กำหนดตัวแปรไว้รับ result ที่ Api return กลับมา
        Dim JsonResult As String

        'กำหนดค่าของ parameter
        Dim dtJsonFlow As New DataJson()
        Dim dt As New DataParameter()

        For i As Integer = 0 To 3
            dt.society = "TOG"
            dt.fileNumber = "1000012"
            dt.supplyJobNumber = "9876543210"
            dt.status = "WIP"
            dt.updateTime = "2019-09-24 10:00:00"
            dt.estimatedDeliveredDate = "2019-09-20"

            dtJsonFlow.jsonFlow.Add(dt)
        Next

        'แปลง DataModel ให้อยู่ในรูปแบบ json
        DataText = JsonConvert.SerializeObject(dtJsonFlow)

        'เรียก Library
        Dim LibApi As New ApiHelper()

        'เรียก function ใน Library
        JsonResult = LibApi.CallApi(PathUrl, DataText, UserName, PassWord, BAsicAuth)

        'กำหนด model ไว้แปลง result ที่ได้รับกลับมาจาก Api ให้อยู่ในรูปแบบ model จะได้นำไปใช้ต่อง่ายขึ้น
        Dim Masterdata As DataModel

        'แปลง result ที่ได้รับกลับมาจาก Api ให้อยู่ในรูปแบบ model
        Masterdata = JsonConvert.DeserializeObject(Of DataModel)(JsonResult)

        Dim connectionString As String = System.Configuration.ConfigurationManager.ConnectionStrings("ThaiOpticDB").ToString()
        Dim connection As New SqlConnection(connectionString)

        Dim SqlCommand As New SqlCommand("INSERT INTO t_thaioptic ( code, message , datetime, description, data) VALUES(@code, @message, @datetime,  @description, @data ) ", connection)

        connection.Open()

        Dim temp As New DataDetail
        SqlCommand.Parameters.AddWithValue("@code", temp.code)
        SqlCommand.Parameters.AddWithValue("@message", temp.message)
        SqlCommand.Parameters.AddWithValue("@datetime", temp.datetime)
        SqlCommand.Parameters.AddWithValue("@description", temp.description)
        SqlCommand.Parameters.AddWithValue("@data", temp.data)

        For Each item As DataDetail In Masterdata.data
            SqlCommand.Parameters("@code").Value = item.code
            SqlCommand.Parameters("@message").Value = item.message
            SqlCommand.Parameters("@datetime").Value = item.datetime
            SqlCommand.Parameters("@description").Value = item.description
            SqlCommand.Parameters("@data").Value = item.data
            SqlCommand.ExecuteNonQuery()
        Next
        connection.Close()
        Console.WriteLine(JsonResult)
        Console.ReadLine()

    End Sub
    Public Class DataModel
        Public code As String
        Public message As String
        Public datetime As String
        Public description As String
        Public data As New List(Of DataDetail)
    End Class
    Public Class DataDetail
        Public code As Integer
        Public message As String
        Public datetime As DateTime
        Public description As String
        Public data As String
    End Class
    Public Class DataParameter
        Public society As String
        Public fileNumber As String
        Public supplyJobNumber As String
        Public status As String
        Public updateTime As String
        Public estimatedDeliveredDate As String
    End Class
    Public Class DataJson
        Public jsonFlow As New List(Of DataParameter)
    End Class
End Module
